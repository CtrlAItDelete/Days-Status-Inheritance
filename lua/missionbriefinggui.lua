local NewLoadoutTab_confirm_pressed = NewLoadoutTab.confirm_pressed
function NewLoadoutTab:confirm_pressed()
    if managers.job:current_job_chain_data() then
		local job_chain = managers.job:current_job_chain_data()
		
		if #job_chain > 1 then
			return
		end
    end
	
	NewLoadoutTab_confirm_pressed(self)
end