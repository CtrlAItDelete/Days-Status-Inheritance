dofile(ModPath .. "base.lua")

Hooks:PreHook(ElementMissionEnd, "on_executed", "DaysStatusInheritance_Inherit_eme", function(self, ...)
	if self._values.state == "success" then
		DaysStatusInheritance:Inherit()
	end
end)