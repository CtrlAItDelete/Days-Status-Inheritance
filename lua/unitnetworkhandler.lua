dofile(ModPath .. "base.lua")

Hooks:PreHook(UnitNetworkHandler, "mission_ended", "DaysStatusInheritance_Inherit_unh", function(self, win, num_is_inside, sender)
	if win then
		DaysStatusInheritance:Inherit()
	end
end)