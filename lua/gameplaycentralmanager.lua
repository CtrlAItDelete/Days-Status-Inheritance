dofile(ModPath .. "base.lua")

Hooks:PreHook(GamePlayCentralManager, "restart_the_game", "DaysStatusInheritance_Restore_rtg", function(self, ...)
	DaysStatusInheritance:SaveCurrent()
end)