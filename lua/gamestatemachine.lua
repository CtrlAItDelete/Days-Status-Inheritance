dofile(ModPath .. "base.lua")

local GameStateMachine_change_state_by_name = GameStateMachine.change_state_by_name
function GameStateMachine:change_state_by_name(state_name, ...)
	if state_name == "victoryscreen" then
		DaysStatusInheritance:Inherit()
	end
	
	GameStateMachine_change_state_by_name(self, state_name, ...)
end