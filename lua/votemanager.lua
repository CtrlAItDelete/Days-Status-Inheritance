dofile(ModPath .. "base.lua")

Hooks:PreHook(VoteManager, "restart", "DaysStatusInheritance_Restore_Restart", function(self, ...)
	DaysStatusInheritance:SaveCurrent()
end)

Hooks:PreHook(VoteManager, "restart_auto", "DaysStatusInheritance_Restore_RestartAuto", function(self, ...)
	DaysStatusInheritance:SaveCurrent()
end)