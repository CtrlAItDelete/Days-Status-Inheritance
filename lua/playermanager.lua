dofile(ModPath .. "base.lua")

local function set_amount(equipment, amount)
	if DaysStatusInheritance.status[equipment] then
		amount = amount - DaysStatusInheritance.status[equipment]
		amount = amount >= 0 and amount or 0
		return amount
	else
		return amount
	end
end

local PlayerManager_add_equipment_amount = PlayerManager.add_equipment_amount
function PlayerManager:add_equipment_amount(equipment, amount, slot)
	DaysStatusInheritance:LoadCurrent()
	DaysStatusInheritance:SaveInherit()
	amount = set_amount(equipment, amount)
	
	PlayerManager_add_equipment_amount(self, equipment, amount, slot)
end

Hooks:PostHook(PlayerManager, "spawned_player", "DaysStatusInheritance_SetStatus", function(self, id, unit)
	if managers.player:player_unit() ~= unit then
		return
	end
	
	local player_unit = managers.player:player_unit()
	if player_unit then
		-- set ammo
		for weapon_id, weapon in ipairs(player_unit:inventory():available_selections()) do
			if alive(weapon.unit) then
				local weapon_base = weapon.unit:base()
				if weapon.unit:base():weapon_tweak_data().categories[1] ~= "saw" then
					if weapon_id == 1 then
						local _, _, ammo_amount = weapon.unit:base():ammo_info()
						local ammo = ammo_amount - DaysStatusInheritance.status["secondary_ammo"]
						weapon_base:ammo_base():set_ammo_total(ammo)
						weapon_base:ammo_base():set_ammo_remaining_in_clip(math.min(weapon_base:ammo_base():get_ammo_max_per_clip(), ammo))
						managers.hud:set_ammo_amount(weapon_id, weapon.unit:base():ammo_info())
					elseif weapon_id == 2 then
						local _, _, ammo_amount = weapon.unit:base():ammo_info()
						local ammo = ammo_amount - DaysStatusInheritance.status["primary_ammo"]
						weapon_base:ammo_base():set_ammo_total(ammo)
						weapon_base:ammo_base():set_ammo_remaining_in_clip(math.min(weapon_base:ammo_base():get_ammo_max_per_clip(), ammo))
						managers.hud:set_ammo_amount(weapon_id, weapon.unit:base():ammo_info())					
					end
				end
			end
		end
		-- set health
		local max_health = player_unit:character_damage():_max_health()
		local health = max_health - DaysStatusInheritance.status["health"]
		player_unit:character_damage():set_health(health)
		
		--set in custody
		if DaysStatusInheritance.in_custody then
			local player = managers.player:local_player()
			managers.player:force_drop_carry()
			managers.statistics:downed({death = true})
			IngameFatalState.on_local_player_dead()
			game_state_machine:change_state_by_name("ingame_waiting_for_respawn")
			player:character_damage():set_invulnerable(true)
			player:character_damage():set_health(0)
			player:base():_unregister()
			player:base():set_slot(player, 0)
		end
	end
end)

local PlayerManager_set_grenade = PlayerManager._set_grenade
function PlayerManager:_set_grenade(params)
	params.amount = params.amount - DaysStatusInheritance.status["grenade"]
	
	PlayerManager_set_grenade(self, params)
end