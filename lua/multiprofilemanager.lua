local MultiProfileManager_set_current_profile = MultiProfileManager.set_current_profile
function MultiProfileManager:set_current_profile(...)
    if managers.job:current_job_chain_data() then
		local job_chain = managers.job:current_job_chain_data()
		
		if #job_chain > 1 then
			return
		end
    end
    
    MultiProfileManager_set_current_profile(self, ...)
end
