dofile(ModPath .. "base.lua")

local PlayerEquipment_use_doctor_bag = PlayerEquipment.use_doctor_bag
function PlayerEquipment:use_doctor_bag()
	local ray = self:valid_shape_placement("doctor_bag")

	if ray then
		DaysStatusInheritance:SaveStatus("doctor_bag")
	end
	
	return PlayerEquipment_use_doctor_bag(self)
end

local PlayerEquipment_use_ammo_bag = PlayerEquipment.use_ammo_bag
function PlayerEquipment:use_ammo_bag()
	local ray = self:valid_shape_placement("ammo_bag")

	if ray then
		DaysStatusInheritance:SaveStatus("ammo_bag")
	end
	
	return PlayerEquipment_use_ammo_bag(self)
end

-- local PlayerEquipment_use_first_aid_kit = PlayerEquipment.use_first_aid_kit
-- function PlayerEquipment:use_first_aid_kit()
	-- local ray = self:valid_shape_placement("first_aid_kit")

	-- if ray then
		-- DaysStatusInheritance:SaveStatus("first_aid_kit")
	-- end
	
	-- return PlayerEquipment_use_first_aid_kit(self)
-- end

local PlayerEquipment_use_bodybags_bag = PlayerEquipment.use_bodybags_bag
function PlayerEquipment:use_bodybags_bag()
	local ray = self:valid_shape_placement("bodybags_bag")

	if ray then
		DaysStatusInheritance:SaveStatus("bodybags_bag")
	end
	
	return PlayerEquipment_use_bodybags_bag(self)
end

Hooks:PreHook(PlayerEquipment, "use_ecm_jammer", "DaysStatusInheritance_SaveEcmJammer", function(self)
	local ray = self:valid_shape_placement("ecm_jammer")
	
	if ray then
		DaysStatusInheritance:SaveStatus("ecm_jammer")
	end
end)

Hooks:PreHook(PlayerEquipment, "use_grenade_crate", "DaysStatusInheritance_SaveGrenadeCrate", function(self)
	local ray = self:valid_shape_placement("grenade_crate")
	
	if ray then
		DaysStatusInheritance:SaveStatus("grenade_crate")
	end
end)

Hooks:PreHook(PlayerEquipment, "use_sentry_gun", "DaysStatusInheritance_SaveSentryGun", function(self)
	local ray = self:valid_shape_placement("sentry_gun")
	
	if ray then
		DaysStatusInheritance:SaveStatus("sentry_gun")
	end
end)

Hooks:PreHook(PlayerEquipment, "use_trip_mine", "DaysStatusInheritance_SaveTripMine", function(self)
	local ray = self:valid_shape_placement("trip_mine")
	
	if ray then
		DaysStatusInheritance:SaveStatus("trip_mine")
	end
end)