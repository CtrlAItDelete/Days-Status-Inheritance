dofile(ModPath .. "base.lua")

Hooks:PostHook(PlayerDamage, "init", "DaysStatusInheritance_SetRevives", function(self, unit)
	DaysStatusInheritance:LoadCurrent()
	local revives = Application:digest_value(self._revives, false) - DaysStatusInheritance.status["revives"]
	revives = revives > 0 and revives or 0

	if revives > 0 then
		DaysStatusInheritance.in_custody = false
	else
		DaysStatusInheritance.in_custody = true
	end
	
	self._revives = Application:digest_value(revives, true)
	
	if revives == 1 then
		managers.environment_controller:set_last_life(true)
	end
end)
