_G.DaysStatusInheritance = _G.DaysStatusInheritance or {}

DaysStatusInheritance.status_path = SavePath .. "dsi_players_status.json"
DaysStatusInheritance.current_path = SavePath .. "dsi_current_status.json"
DaysStatusInheritance.in_custody = false
DaysStatusInheritance.status = {
	doctor_bag = 0,
	ammo_bag = 0,
	first_aid_kit = 0,
	bodybags_bag = 0,
	ecm_jammer = 0,
	grenade_crate = 0,
	sentry_gun = 0,
	trip_mine = 0,
	primary_ammo = 0,
	secondary_ammo = 0,
	revives = 0,
	health = 0,
	grenade = 0
}

function DaysStatusInheritance:Save(path)
	if path == 1 then
		local file = io.open(self.status_path, "w+")
		if file then
			file:write(json.encode(self.status))
			file:close()
		end
	elseif path == 2 then
		local file = io.open(self.current_path, "w+")
		if file then
			file:write(json.encode(self.status))
			file:close()
		end
	end
end

function DaysStatusInheritance:SaveStatus(equipment)

	if equipment then
		self.status[equipment] = self.status[equipment] + 1
	else
		for name ,_ in pairs(self.status) do
			self.status[name] = 0
		end
	end
	
	DaysStatusInheritance:Save(1)
end

function DaysStatusInheritance:SaveInherit()
	DaysStatusInheritance:Save(1)
	DaysStatusInheritance:Save(2)
end

function DaysStatusInheritance:SaveCurrent()
	self:LoadCurrent()
	
	DaysStatusInheritance:Save(1)
end

function DaysStatusInheritance:Load()
	local file = io.open(self.status_path, "r")
	if file then
		local players_state = json.decode(file:read("*all"))
		for name, status in pairs(players_state) do
			self.status[name] = status
		end
		file:close()
	end
end

function DaysStatusInheritance:LoadCurrent()
	local file = io.open(self.current_path, "r")
	if file then
		local players_state = json.decode(file:read("*all"))
		for name, status in pairs(players_state) do
			self.status[name] = status
		end
		file:close()
	end
end

function DaysStatusInheritance:GetUsedAmmo()
	local player_unit = managers.player:player_unit()
	self._weapon_ammo = {}
	
	if player_unit then
		for id, weapon in ipairs(managers.player:player_unit():inventory():available_selections()) do
			if alive(weapon.unit) then
				local _, _, amount = weapon.unit:base():ammo_info()
				local max_ammo = weapon.unit:base():get_ammo_max()
				local amount = max_ammo - amount
				self._weapon_ammo[id] = amount
			end
		end
	else
		self._weapon_ammo[1] = 0
		self._weapon_ammo[2] = 0
	end
	
	return self._weapon_ammo
end

function DaysStatusInheritance:SetWeaponsAmmo()
	local weapon_ammo = self:GetUsedAmmo()
	self.status["primary_ammo"] =	weapon_ammo[2]
	self.status["secondary_ammo"] = weapon_ammo[1]
end

function DaysStatusInheritance:SetRevives()
	local player_unit = managers.player:player_unit()
	
	if player_unit then
		local is_one_down = Global.game_settings.one_down
		local LIVES_INIT = tweak_data.player.damage.LIVES_INIT
		local additional_lives = managers.player:upgrade_value("player", "additional_lives", 0)
		local revives = (is_one_down and 2 or LIVES_INIT) + additional_lives
		self.status["revives"] = revives - Application:digest_value(player_unit:character_damage()._revives, false)
	else
		local in_custody = managers.trade:is_peer_in_custody(managers.network:session():local_peer():id())
		
		if in_custody then
			self.status["revives"] = 5
		else
			self.status["revives"] = 0
		end
	end
end

function DaysStatusInheritance:SetHealth()
	local player_unit = managers.player:player_unit()
	
	if player_unit then
		local max_health = player_unit:character_damage():_max_health()
		local health = player_unit:character_damage():get_real_health()
		
		if health ~= max_health then
			self.status["health"] = max_health - health
		end
	else
		self.status["health"] = 0
	end
end

function DaysStatusInheritance:SetGrenade()
	local max_grenades = managers.player:get_max_grenades()
	local grenade_amount = managers.player:get_grenade_amount(managers.network:session():local_peer():id())
	local used_grenades = max_grenades - grenade_amount
	self.status["grenade"] = used_grenades
end

function DaysStatusInheritance:Inherit()
	self:SetWeaponsAmmo()
	self:SetRevives()
	self:SetHealth()
	self:SetGrenade()
	self:SaveInherit()
end

function DaysStatusInheritance:Resetting()
	DaysStatusInheritance:SaveStatus()
	DaysStatusInheritance:Save(2)
end
